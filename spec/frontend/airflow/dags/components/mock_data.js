export const mockDags = [
  {
    id: 1,
    project_id: 7,
    created_at: '2023-01-05T14:07:02.975Z',
    updated_at: '2023-01-05T14:07:02.975Z',
    has_import_errors: false,
    is_active: false,
    is_paused: true,
    next_run: '2023-01-05T14:07:02.975Z',
    dag_name: 'Dag number 1',
    schedule: 'Manual',
    fileloc: '/opt/dag.py',
  },
  {
    id: 2,
    project_id: 7,
    created_at: '2023-01-05T14:07:02.975Z',
    updated_at: '2023-01-05T14:07:02.975Z',
    has_import_errors: false,
    is_active: false,
    is_paused: true,
    next_run: '2023-01-05T14:07:02.975Z',
    dag_name: 'Dag number 2',
    schedule: 'Manual',
    fileloc: '/opt/dag.py',
  },
  {
    id: 3,
    project_id: 7,
    created_at: '2023-01-05T14:07:02.975Z',
    updated_at: '2023-01-05T14:07:02.975Z',
    has_import_errors: false,
    is_active: false,
    is_paused: true,
    next_run: '2023-01-05T14:07:02.975Z',
    dag_name: 'Dag number 3',
    schedule: 'Manual',
    fileloc: '/opt/dag.py',
  },
  {
    id: 4,
    project_id: 7,
    created_at: '2023-01-05T14:07:02.975Z',
    updated_at: '2023-01-05T14:07:02.975Z',
    has_import_errors: false,
    is_active: false,
    is_paused: true,
    next_run: '2023-01-05T14:07:02.975Z',
    dag_name: 'Dag number 4',
    schedule: 'Manual',
    fileloc: '/opt/dag.py',
  },
  {
    id: 5,
    project_id: 7,
    created_at: '2023-01-05T14:07:02.975Z',
    updated_at: '2023-01-05T14:07:02.975Z',
    has_import_errors: false,
    is_active: false,
    is_paused: true,
    next_run: '2023-01-05T14:07:02.975Z',
    dag_name: 'Dag number 5',
    schedule: 'Manual',
    fileloc: '/opt/dag.py',
  },
];
